package com.banking.gateway.config;

import com.banking.gateway.filter.JwtFilter;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class RouteConfig {

  private final JwtFilter jwtFilter;

  @Value("${app.services.auth}")
  private String authURI;

  @Value("${app.services.products}")
  private String productsURI;

  @Value("${app.services.payments}")
  private String paymentsURI;

  @Bean
  public RouteLocator routeLocator(RouteLocatorBuilder builder) {
    return builder.routes()
        .route("auth", r -> r
            .path("/api/v1/auth/passcode/**")
            .or()
            .path("/api/v1/auth/otp/**")
//            .uri(authURI + ":8082"))
            .uri(authURI))
        .route("auth-details", r -> r
            .path("/api/v1/file/**")
            .or()
            .path("/api/v1/profile/**")
            .filters(f -> f.filter(jwtFilter))
//            .uri(authURI + ":8082"))
            .uri(authURI))
        .route("products", r -> r
            .path("/api/v1/cars/**")
            .uri(productsURI))
//            .uri(productsURI + ":8083"))
        .build();
  }

}
