package com.banking.gateway.filter;


import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import java.util.Date;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

@Slf4j
@Component
public class JwtFilter implements GatewayFilter {
  @Value("${app.jwt.secret}")
  private String SECRET_KEY;


  @Override
  public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {

    try {
      String token = extractJwtFromRequest(exchange.getRequest().getHeaders().getFirst("Authorization"));

      if (token == null || !validateToken(token)) {
        log.error("Invalid or missing JWT token");
        return handleUnauthorized(exchange);
      }

      Jws<Claims> claims = Jwts.parser()
          .setSigningKey(SECRET_KEY)
          .parseClaimsJws(token);

      String username = claims.getBody().get("username", String.class);
      Long userId = claims.getBody().get("userId", Long.class);
      exchange.getRequest().mutate().header("User-Mobile-Number", username);
      exchange.getRequest().mutate().header("User-Id", String.valueOf(userId));
      return chain.filter(exchange);

    } catch (Exception e) {
      log.error("Error processing JWT token", e);
      return handleUnauthorized(exchange);
    }

  }

  private Mono<Void> handleUnauthorized(ServerWebExchange exchange) {
    ServerHttpResponse response = exchange.getResponse();
    response.setStatusCode(HttpStatus.UNAUTHORIZED);

    return response.setComplete();
  }
  private String extractJwtFromRequest(String authorizationHeader) {
    if (authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
      return authorizationHeader.substring(7);
    }
    return null;
  }
  private boolean validateToken(String token) {
    try {
      Jws<Claims> claimsJws = Jwts.parserBuilder().setSigningKey(SECRET_KEY).build().parseClaimsJws(token);

      return !claimsJws.getBody().getExpiration().before(new Date());
    } catch (Exception e) {
      return false;
    }
  }


}
